#pragma once

#include <algorithm>
#include <cstdlib>
#include <iterator>
#include <string>

using std::begin;
using std::end;
using std::remove_if;
using std::string;
using std::tolower;
using std::transform;

//------------------------------------------------------------------------------
/**
 * @brief returns true if c isn't whitespace
 */
bool not_space(const char c)
{
  return !isspace(static_cast<unsigned char>(c));
}

/**-----------------------------------------------------------------------------
 * @brief mutates s, removing leading whitespace
 */
void trim_lt(std::string& s)
{
  s.erase(begin(s), find_if(begin(s), end(s), not_space));
}

/**-----------------------------------------------------------------------------
 * @brief mutates s, removing trailing whitespace
 */
void trim_rt(std::string& s)
{
  s.erase(find_if(rbegin(s), rend(s), not_space).base(), end(s));
}

/**-----------------------------------------------------------------------------
 * @brief remove leading and trailing whitespace from string
 */
string trim(std::string s)
{
  trim_lt(s);
  trim_rt(s);

  return s;
}

/**-----------------------------------------------------------------------------
 * @brief return an all-lowercase string
 */
std::string to_lower(std::string s)
{
  transform(begin(s), end(s), begin(s),
            [](unsigned char c) { return tolower(c); });
  return s;
}

/**-----------------------------------------------------------------------------
 * @brief consolidate consecutive chars ch in a string down to just one ch per
 */
std::string squash(std::string s, const char ch = ' ')
{
  char prev{'\0'};

  s.erase(remove_if(begin(s), end(s),
                    [&prev, ch](const auto c) {
                      if ((c == prev) && (c == ch))  // have a dupe
                        return true;
                      else {
                        prev = c;
                        return false;
                      }
                    }),
          end(s));
  return s;
}

/**-----------------------------------------------------------------------------
 * @brief a simple lambda to filter non-alpha characters
 */
auto not_space_alpha = [](const unsigned char c) {
  if (isspace(c))
    return false;  // keep spaces
  else
    return !isalpha(c);
};

/**-----------------------------------------------------------------------------
 * @brief strip any non-alpha chars from string, except spaces
 */
std::string to_alpha(std::string s)
{
  s.erase(remove_if(begin(s), end(s), not_space_alpha), end(s));
  return s;
}

/**-----------------------------------------------------------------------------
 * @brief perform general cleanup on a string
 * @note this is used to prep for matching player's answers to riddle's answers
 */
std::string clean_str(std::string s)
{
  // clang-format off
  return to_lower(              // all lowercase
             to_alpha(          // alpha chars only
                 trim(          // remove leading/trailing spaces
                     squash(s)  // remove any extra, consecutive spaces
         )));
  // clang-format on
}

// Copyright (2020)
// License (MIT)  https://opensource.org/licenses/MIT
