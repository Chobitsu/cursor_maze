/*
 filename: maze.cpp
 ----------
 this file is a very simple, ncurses-based maze game expanded from a basic idea
 in 'Functional Programming in C++', pp106-109.

 Usage:
 ----------
 [w,a,s,d] or arrow keys to move
 [q] to quit

 Build & run:
 ----------
 dependencies:  c++17, ncurses
 note: everything here should be saved as a single text file named 'maze.cpp',
 then from the terminal enter:

  c++ maze.cpp -std=c++17 -lncurses -lpthread -o maze

 note: you then run the maze game from the terminal, enter:

  ./maze

*/

// TODO:
//---
// DESIGN: have a very short countdown clock for a life. use 't' powerups  //1/2
// DESIGN: add an overall timer for the entire maze completion

//---
#include <algorithm>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <iterator>
#include <limits>
#include <random>
#include <sstream>
#include <string>
#include <thread>
#include <tuple>
#include <utility>
#include <vector>

//---
#include <ncurses.h>

#include "Indexer.hpp"
#include "text_proc.hpp"

//---
using std::begin;
using std::cbegin;
using std::cend;
using std::distance;
using std::end;
using std::exit;
using std::find;
using std::find_if;
using std::isalpha;
using std::isspace;
using std::pair;
using std::rbegin;
using std::rend;
using std::sqrt;
using std::string;
using std::stringstream;
using std::to_string;
using std::tolower;
using std::tuple;
using std::vector;
using std::this_thread::sleep_for;

using namespace std::chrono_literals;

//---
using uint = unsigned;
using Row = uint;
using Col = uint;
constexpr auto dblmax{std::numeric_limits<double>::max()};

// fwd decls
//---
struct Maze;
struct Posn;
void proc_events(Maze&, const Posn&);
void draw(Maze&, const Posn&);
void draw_status(Maze&);
void leave(Maze&);

// messaging positions
//---
const uint hdr1_base{16};
const uint hdr2_base{50};  // <- change this num to move messages left/right
const uint stat_base{hdr2_base};
const uint msg_base{hdr2_base};
const uint stat_off{6};
uint msg_off{stat_off + 1};

/**-----------------------------------------------------------------------------
 * @brief simple message then exit
 */
void game_over(Maze& m)
{
  attron(A_REVERSE);
  mvprintw(stat_off + 3, stat_base, "%s", " game over ");
  leave(m);
}

/**-----------------------------------------------------------------------------
 * @brief simple message then exit
 */
void quit(Maze& m)
{
  mvprintw(stat_off + 2, stat_base, "%s", "player quit");
  game_over(m);
}

/**-----------------------------------------------------------------------------
 * @brief quick-exit
 */
void draw_n_leave(Maze& m, const Posn& p)
{
  draw(m, p);
  leave(m);
}

/**-----------------------------------------------------------------------------
 * @brief leaving the 0-th row intact, clear the rest of the ncurses screen
 */
void clear_scr()
{
  move(1, 0);
  clrtobot();
}

/**-----------------------------------------------------------------------------
 * @brief simple enum to track directions
 */
enum Dir { up, dn, lt, rt, none };

/**-----------------------------------------------------------------------------
 * @brief the Posn definition, a position in the maze 'universe'
 * @note position indexing is top-down (rows), then left-right (cols)
 * @note uses zero-based indexing
 * @note just like ncurses
 */
struct Posn {
  Posn()
  {
  }
  Posn(const Row i, const Col j) : i{i}, j{j}
  {
  }
  Posn(const Posn& orig, const Dir dir)
      // DESIGN: this is being too clever
      : i{dir == up ? orig.i - 1 : (dir == dn ? orig.i + 1 : orig.i)},
        j{dir == lt ? orig.j - 1 : (dir == rt ? orig.j + 1 : orig.j)}
  {
  }

  /**------------
   * @brief given another position, return this position's distance to it
   */
  double distance(const Posn& other) const
  {
    uint i_ofst{i - other.i};
    uint j_ofst{j - other.j};

    return sqrt((i_ofst * i_ofst) + (j_ofst * j_ofst));  // a^2 + b^2 = c^2
  }

  // Boolean comparisons
  //---
  bool operator==(const Posn& other) const
  {
    return (i == other.i) && (j == other.j);
  }
  bool operator!=(const Posn& other) const
  {
    return (i != other.i) || (j != other.j);
  }

  //---
  Row i{};
  Col j{};
};

/**-----------------------------------------------------------------------------
 * @brief simple struct providing named fields
 */
struct Level {
  string name;
  string rows;
};

/**-----------------------------------------------------------------------------
 * @brief a starter collection of 4 simple levels
 * @note edit/add to these levels to change/create new levels for your game
 * build up each row using these particular types of chars, as in these examples
 * @note keep in mind the rows are all doubled in width by the draw() function,
 * it's recommended to keep the column count for your levels low enough to not
 * interfere w/ info display panels on the right, etc (say, ~20-25 actual chars)
 * ----------
 *  _  maze start/finish   (1 only, must be in level 0)
 *  .  egg                 (1 only, usually in the final level)
 *  1,2,3  keys            (3 only, spread out among levels)
 *  M  fight monster
 *  ?  answer riddle
 *  $  bag loot
 *  h  powerup, health
 *  t  powerup, time
 *  <  go prev level       (ignored on top level)
 *  >  go next level       (ignored on bottom level)
 *  o  pitfall
 *  #  catasplosionator
 *  *  wall
 *
 * @design this approach could easily be expanded out to include many separate,
 * large, maze-level text files to read in from disk & play. it's included here
 * this way as a small example just to get you started.
 */
struct Levels {
  // NOTE: the example levels here contain these objectives totals;
  // 1 egg
  // 3 keys
  // 6 monsters
  // 7 riddles

  // level 0
  const Level a{"Neptune", R"(
**********
*M?     t*
* *<>$   *
* ****** *
* *      *
* * ********
*>*???????_*
************
    )"};

  // level 1
  const Level b{"Uranus", R"(
**********
*<*  $ ?>*
* * * * <*
* * * * *****
* h * *1* t>*
* *** *** ***
*M         ?*
*************
    )"};

  // level 2
  const Level c{"Saturn", R"(
 **********
 *t  ?   >*
 *   ?   **
 *   M<  *
 *   2   *
**  $o$  *
*<M  $  h*
**********
      )"};

  // level 3
  const Level d{"Jupiter", R"(
********
*#    ?*
*  *** **
*   3* h*
* **** ****
*  t*  M .*
** **********
*M   <>    $*
*** **** ****
*<   ?   o*
***********
    )"};

  /**------------
   * @brief return a container holding the Levels
   */
  vector<Level> all_levels() const
  {
    vector<Level> res{};

    res.push_back(a);
    res.push_back(b);
    res.push_back(c);
    res.push_back(d);
    return res;
  }

};  // struct Levels

/**-----------------------------------------------------------------------------
 * @brief simple struct providing named fields
 */
struct Riddle {
  string question;
  string answer;
};

/**-----------------------------------------------------------------------------
 * @brief a starter collection of silly riddles
 *
 * @design this approach could easily be expanded out to include many lists of
 * silly riddles inside text files to read in from disk & play. it's included
 * here this way as a small example just to get you started.
 */
struct Riddles {
  const Riddle a{"What is the Airspeed Velocity of an Unladen Swallow?",
                 "African or European swallow?"};
  const Riddle b{
      "What five-letter word becomes shorter if you add two letters to it?",
      "Short"};
  const Riddle c{"Why is an island like the letter T?",
                 "They’re both in the middle of water"};
  const Riddle d{"How many seconds are there in a year?", "Twelve"};  // Jan 2..
  const Riddle e{"foo", "bar"};
  //
  const Riddle f{"foo", "bar"};
  const Riddle g{"foo", "bar"};
  const Riddle h{"foo", "bar"};
  const Riddle i{"foo", "bar"};
  const Riddle j{"foo", "bar"};

  /**------------
   * @brief return a container holding the Riddles
   */
  vector<Riddle> all_riddles() const
  {
    vector<Riddle> res{};

    res.push_back(a);
    res.push_back(b);
    res.push_back(c);
    res.push_back(d);
    res.push_back(e);
    // res.push_back(f);
    // res.push_back(g);
    // res.push_back(h);
    // res.push_back(i);
    // res.push_back(j);
    return res;
  }
};

/**-----------------------------------------------------------------------------
 * @brief return a random number from 0 to lim
 */
uint rand_of(const uint lim)
{
  // Will be used to obtain a seed for the random number engine
  std::random_device rd;
  std::mt19937 gen(rd());  // Standard mersenne_twister_engine seeded with rd()
  std::uniform_int_distribution<> dist(0, lim);

  return dist(gen);
}

/**-----------------------------------------------------------------------------
 * @brief the Maze definition, consisting of multiple levels
 * @note position indexing is top-down (rows), then left-right (cols)
 * @note uses zero-based indexing
 * @note just like ncurses
 */
struct Maze {
  Maze()
  {
    // LEVELS
    //------------
    Levels maze{};  // get a copy of the example levels

    uint all_monsters_cnt{}, all_riddles_cnt{};
    uint lvl_idx{}, row_idx{};
    bool strt_grd{}, have_mz_strt{};

    vector<Posn> prev_posns{}, next_posns{};
    vector<string> curr_lvl_dat{};

    // first thing, pre-process and store all the levels locally
    //------------
    for (const auto& lvl : maze.all_levels()) {
      //
      stringstream rows{lvl.rows};
      string row{};

      // read all the rows of chars for each level
      while (getline(rows, row)) {
        //
        // incrementally store each row's string of chars, for collection into
        // the master collection's tuple for this level
        curr_lvl_dat.push_back(row);

        // find all the 'go prev' (or maze start) posns (if any are present) in
        // this row, for storage in the master collection's tuple for this level
        auto prevs{find_all_prevs(row, row_idx, lvl_idx)};
        for (const auto& p : prevs) {
          prev_posns.push_back(p);
          if (lvl_idx == 0) have_mz_strt = true;  // found maze start on level 0
        }

        // find all the 'go next' posns (if any are present) in this row, for
        // storage in the master collection's tuple for this level
        auto nexts{find_all_nexts(row, row_idx)};
        for (const auto& p : nexts) {
          next_posns.push_back(p);
        }

        // sum running grand totals for overall objectives while we're here
        // -ie, 'count up all the monsters in the maze, one row at a time'
        all_monsters_cnt += count(cbegin(row), cend(row), 'M');
        all_riddles_cnt += count(cbegin(row), cend(row), '?');

        ++row_idx;

      }  // for all rows

      // if it was found (on level 0), store the maze's primary start position.
      // program errors out otherwise.
      if (!strt_grd) {
        if (have_mz_strt) {
          maze_start = prev_posns[0];  // start posn will be the first element
          strt_grd = true;
        }
        else {
          mvprintw(msg_off++, msg_base, "ERROR: no maze start char '_'");
          mvprintw(msg_off++, msg_base, "found in level 0, exiting");
          draw_n_leave(*this, {0, 0});
        }
      }

      // store the parsed data for this level into the maze's master all-levels
      // container (ie, pack up the tuple for this level)
      all_lvls.push_back({lvl.name, curr_lvl_dat, prev_posns, next_posns});

      // prep for next loop
      prev_posns.clear();
      next_posns.clear();
      curr_lvl_dat.clear();
      ++lvl_idx;
      row_idx = 0;

    }  // for all levels

    // next, resize the objectives flag containers now that we have total counts
    //------------
    monsters = vector<bool>(all_monsters_cnt);
    riddles = vector<bool>(all_riddles_cnt);

    // RIDDLES
    //------------
    Riddles riddles{};  // get a copy of the example riddles

    // first thing, pre-process and store all the riddles locally
    // -basically, clean up the answer part as much as feasible
    //------------
    for (auto& riddle : riddles.all_riddles()) {
      all_riddles.emplace_back(
          pair<string, string>{riddle.question, clean_str(riddle.answer)});
    }

    // finally, start the game at level 0
    //------------
    set_level(0);

  }  // Maze ctor

  //------------
  vector<pair<string, string>> all_riddles{};
  pair<string, string> random_riddle()
  {
    auto rnd_idx{rand_of(all_riddles.size() - 1)};
    return all_riddles.at(rnd_idx);
  }

  /**------------
   * @brief return a vector of all positions where a 'go prev' char is found
   * inside the row
   */
  vector<Posn> find_all_prevs(const string& row, const uint row_idx,
                              const uint lvl_idx)
  {
    vector<Posn> res{};
    auto it{begin(row)};

    while (it != cend(row)) {
      //
      it = find_if(it, cend(row), [lvl_idx](const auto c) {
        if (lvl_idx != 0)  // only return '<' posn for levels other than zero
          return c == '<';
        else
          return c == '_';
      });

      if (it != cend(row)) {
        auto col_idx{static_cast<uint>(it - cbegin(row))};
        res.emplace_back(Posn{row_idx, col_idx});

        if (lvl_idx == 0) break;  // we already have the maze's start posn now

        ++it;  // begin the next find_if() search at the next char
      }
    }
    return res;
  }

  /**------------
   * @brief return a vector of all positions where a 'go next' char is found
   * inside the row
   */
  vector<Posn> find_all_nexts(const string& row, const uint row_idx)
  {
    vector<Posn> res{};
    auto it{begin(row)};

    while (it != cend(row)) {
      //
      it = find(it, cend(row), '>');

      if (it != cend(row)) {
        auto col_idx{static_cast<uint>(it - cbegin(row))};
        res.emplace_back(Posn{row_idx, col_idx});
        ++it;  // begin the next find() search at the next char
      }
    }
    return res;
  }

  // Lives mgmt
  //------------
  vector<bool> lives{true, true, true};  // 3 lives (only)
  void add_life()
  {
    auto it = find(begin(lives), end(lives), false);
    if (it != cend(lives)) *it = true;
  }
  bool lose_life()
  {
    auto it{find(rbegin(lives), rend(lives), true)};  // turns off from the end
    *it = false;

    // will be true if all lives are now gone
    return find(cbegin(lives), cend(lives), true) == cend(lives);
  }
  //
  // if this 1sec countdown timer reaches zero, player loses a life
  int timer_len{26};  // float an extra sec, timer displays after decr
  int life_tick{timer_len};

  // Scoring
  //------------
  int health{100}, loot{50}, points{};

  void lose_everything()  // Insult to injury!! :^)
  {
    health = loot = points = 0;

    for (uint i{0}; i < lives.size(); ++i) {
      lives[i] = false;
    }
    for (uint i{0}; i < keys.size(); ++i) {
      keys[i] = false;
    }
    for (uint i{0}; i < monsters.size(); ++i) {
      monsters[i] = false;
    }
    for (uint i{0}; i < riddles.size(); ++i) {
      riddles[i] = false;
    }

    egg = false;
  }

  // Objectives
  //------------
  bool egg{false};                         // 1 egg (only)
  vector<bool> keys{false, false, false};  // 3 keys (only)
  vector<bool> monsters{};                 // unlimited monsters
  vector<bool> riddles{};                  // unlimited riddles

  /**------------
   * @brief wrapper for egg flag
   */
  bool have_egg() const
  {
    return egg;
  }

  /**------------
   * @brief have we gotten all the X's in the collection yet?
   */
  bool have_all(const vector<bool>& coll) const
  {
    return find(cbegin(coll), cend(coll), false) == cend(coll);  // if all true
  }

  // DESIGN: ditch these 4 funcs, replacing them w/ 3 that perform the chore
  // directly, then adjust the 3 callers to match.

  /**------------
   * @brief 3 wrappers to simplify calling code
   */
  uint next_key() const
  {
    return next_flag(keys);
  }
  uint next_monster() const
  {
    return next_flag(monsters);
  }
  uint next_riddle() const
  {
    return next_flag(riddles);
  }
  /**------------
   * @brief return the index of the next false flag within the vector
   * @note used by the orig caller to set that indexed flag
   */
  uint next_flag(const vector<bool>& coll) const
  {
    uint res{};

    for (const auto e : coll) {
      if (e) ++res;
    }
    return res;
  }

  //------------
  // the Maze's master all-levels container
  vector<tuple<string, vector<string>, vector<Posn>, vector<Posn>>> all_lvls{};
  // index for the level we're currently on
  uint curr_idx{};

  /**------------
   * @brief return the data for the current level
   */
  vector<string>& curr_lvl()
  {
    auto& [n, l, pp, np]{all_lvls[curr_idx]};  // unpack level

    return l;
  }

  /**------------
   * @brief do housekeeping for a level change
   */
  void set_level(const uint targ_idx)
  {
    curr_idx = targ_idx;
    const auto& [n, l, pp, np]{all_lvls[curr_idx]};  // unpack name

    clear_scr();  // TEMP: disable for testing
    mvprintw(0, hdr1_base, " - welcome to level %d '%s'           ", curr_idx,
             n.c_str());
  }

  /**------------
   * @brief given a source position and a collection of positions, return the
   * index of the closest position in the collection to the source.
   */
  int clst_posn_idx(const Posn& p, const vector<Posn>& coll)
  {
    int res{-1};             // sentinel marks location failure, check in caller
    double closest{dblmax};  // measurement starts out from 'infinity' away

    for (const auto& [idx, p2] : indexed(coll)) {  // wrap the coll w/ indexing
      auto dist{p.distance(p2)};
      if (dist < closest) {
        closest = dist;
        res = idx;
      }
    }
    return res;
  }

  /**------------
   * @brief restarts the game recursion beginning at closest position within the
   * collection param
   */
  void jmp_clst_posn(const Posn& p, const vector<Posn>& coll)
  {
    auto jmp_idx{clst_posn_idx(p, coll)};

    if (jmp_idx != -1)  // ensures a go-spot was found on the target level
      proc_events(*this, coll[jmp_idx]);
    else {
      mvprintw(msg_off++, msg_base, "Sorry, the other level doesn't");
      mvprintw(msg_off++, msg_base, "have a proper place to land yet?");
      draw_n_leave(*this, p);
    }
  }

  //------------
  bool prev_grd{}, next_grd{};

  /**------------
   * @brief request transport up to higher level
   */
  void prev_level(const Posn& p)
  {
    if (curr_idx > 0) {  // must be 'below' the top level

      if (!prev_grd) {  // miss first draw tick (no ping-ponging)
        const auto targ_idx{--curr_idx};
        set_level(targ_idx);

        // jump to closest '>' above
        //------------
        next_grd = true;  // prevents ping-ponging:  next->prev->next...
        const auto [n, l, pp, np]{all_lvls[targ_idx]};  // unpack next_posns
        jmp_clst_posn(p, np);
      }
      else
        prev_grd = false;  // clear guard flag
    }
  }

  /**------------
   * @brief request transport down to lower level
   */
  void next_level(const Posn& p)
  {
    if (curr_idx < all_lvls.size() - 1) {  // must be 'above' the bottom level
                                           // NOTE: -1 b/c target == ++curr_idx

      if (!next_grd) {  // miss first draw tick (no ping-ponging)
        const auto targ_idx{++curr_idx};
        set_level(targ_idx);

        // jump to closest '<' below
        //------------
        prev_grd = true;  // prevents ping-ponging:  prev->next->prev...
        const auto [n, l, pp, np]{all_lvls[targ_idx]};  // unpack prev_posns
        jmp_clst_posn(p, pp);
      }
      else
        next_grd = false;  // clear guard flag
    }
  }

  //------------
  Posn maze_start{};

  /**------------
   * @brief wrapper for maze start position
   */
  Posn start_posn() const
  {
    return maze_start;
  }

  /**------------
   * @brief comparison with maze start position
   */
  bool at_maze_exit(const Posn& p) const
  {
    return p == maze_start;  // finish where you began :^)
  }

  /**------------
   * @brief wrapper to test for a wall char
   */
  bool is_wall(const Posn& p)
  {
    const auto& l{curr_lvl()};
    const auto c{l.at(p.i).at(p.j)};

    return c == '*';
  }

  //---

  Posn last_move{};

  bool timer_active{true};

  bool bomb_assploded{false};

};  // struct Maze

/**-----------------------------------------------------------------------------
 * @brief perform cleanup & pause before exit
 */
void leave(Maze& m)
{
  m.timer_active = false;

  refresh();
  getchar();
  endwin();
  exit(0);
}

/**-----------------------------------------------------------------------------
 * @brief drop one or all lives
 */
void die(Maze& m, const Posn& p, bool all_lives = false)
{
  mvprintw(stat_off + 2, stat_base, "%s", "player died");

  if (m.lose_life())  // lose_life() both drops a life & checks if all are gone
    all_lives = true;

  if (all_lives) {
    draw(m, p);
    game_over(m);
  }
  else {
    // heh, gotta cover those funeral expenses bro
    m.points -= 5'000;
    m.loot -= 2'000;
    m.health = 100;  // reset next life's health back to full
    draw_status(m);
  }
}

/**-----------------------------------------------------------------------------
 * @brief are you alright player? do you need us to call someone for you?
 */
void fall(Maze& m, const Posn& p)
{
  mvprintw(stat_off + 1, stat_base, "%s", "you fell into a 300' well!");
  die(m, p);
}

/**-----------------------------------------------------------------------------
 * @brief you should have stayed in bed today :^)
 */
void blowup(Maze& m, const Posn& p)
{
  mvprintw(stat_off + 1, stat_base, "%s",
           "you set off the catasplosionator!!!");

  // DESIGN: blow up the maze structure, clear status, and show other mess
  // ...
  // draw(m.bomb_assploded) ??

  m.lose_everything();
  m.bomb_assploded = true;
  die(m, p, true);  // true flag ends game
}

/**-----------------------------------------------------------------------------
 * @brief restore some health points
 */
void gain_health(Maze& m, char& c)
{
  mvprintw(stat_off + 1, stat_base, "%s", "you got more health       ");

  m.health += 33;
  if (m.health > 100) m.health = 100;  // 100 health limit cap

  c = ' ';
}

/**-----------------------------------------------------------------------------
 * @brief add a little time to the life countdown clock
 */
void gain_time(Maze& m, char& c)
{
  mvprintw(stat_off + 1, stat_base, "%s", "you got more time         ");

  m.life_tick += m.timer_len - 10;          // add ~15secs to the clock
  if (m.life_tick >= 31) m.life_tick = 31;  // ~30secs limit cap

  c = ' ';
}

/**-----------------------------------------------------------------------------
 * @brief pocket some cash to blow at the club tonight
 */
void bag_loot(Maze& m, char& c)
{
  mvprintw(stat_off + 1, stat_base, "%s", "you bagged some loot      ");

  m.loot += 350;  // DESIGN: randomize loot amount

  c = ' ';
}

// uint score_answer(auto foo, auto bar){
// }

std::string get_answer(const std::string& a)
{
  return "";  // tmp
}

/**-----------------------------------------------------------------------------
 * @brief answer a riddle. no fair using Google!
 */
void answer(Maze& m, char& c)
{
  if (m.loot >= 150) m.loot -= 150;  // pay to play, bro
  // else {
  //   mvprintw(msg_off++, msg_base, "sorry player, it takes $150");
  //   mvprintw(msg_off++, msg_base, "loot to play the riddle game");
  //   return;
  // }

  // DESIGN:
  auto [q, a]{m.random_riddle()};

  // ask question, then...

  auto foo = get_answer(q);

  // auto bar = score_answer(foo, a);
  // if (bar >= 75) {
  //   (success, use below code) ...v
  // }
  // else {
  //   failed, show right answer, deduct points, (recharge loot) & try again ...
  //   -probably just recurse here
  // }

  // mvprintw(stat_off + 1, stat_base, "%s", "you answered a riddle     ");

  m.loot += 250;
  m.points += 500;

  m.riddles[m.next_riddle()] = true;
  c = ' ';  // clear the char
}

/**-----------------------------------------------------------------------------
 * @brief fight a monster. hope you brought your game m80...
 */
void fight(Maze& m, const Posn& p, char& c)
{
  // DESIGN:
  // ... 1st, defeat the monster, then

  mvprintw(stat_off + 1, stat_base, "%s", "you defeated a monster    ");

  m.loot += 500;
  m.points += 200;

  m.health -= 26;
  if (m.health <= 0) die(m, p);

  m.monsters[m.next_monster()] = true;
  c = ' ';
}

/**-----------------------------------------------------------------------------
 * @brief obtain one of the three keys
 */
void get_key(Maze& m, char& c)
{
  mvprintw(stat_off + 1, stat_base, "%s", "you got a key!            ");

  m.points += 5'000;
  m.loot += 2'500;

  m.keys[m.next_key()] = true;
  c = ' ';
}

/**-----------------------------------------------------------------------------
 * @brief obtain the only egg. it's a really glowey thingy that earns you a
 * wizard's hat in the end, so gambatte friend!
 */
void get_egg(Maze& m, char& c)
{
  mvprintw(stat_off + 1, stat_base, "%s", "you got the egg!!         ");

  m.points += 25'000;
  m.loot += 15'000;

  m.egg = true;
  c = ' ';
}

/**-----------------------------------------------------------------------------
 * @brief wrapper to transport player to next higher level in the maze
 */
void goto_prev(Maze& m, const Posn& p)
{
  m.prev_level(p);
}

/**-----------------------------------------------------------------------------
 * @brief wrapper to transport player to next lower level in the maze
 */
void goto_next(Maze& m, const Posn& p)
{
  m.next_level(p);
}

/**-----------------------------------------------------------------------------
 * @brief switchboard wrapper to process 'items' at the player's current posn
 */
void check_posn(Maze& m, const Posn& p)
{
  // access the underlying char ref located at this position in the maze
  auto& l{m.curr_lvl()};
  auto& c{l.at(p.i).at(p.j)};

  switch (c) {
    case ' ':  // nop on empty space (the most common case ofc)
      break;
    case '?':  // riddle
      answer(m, c);
      break;
    case 'M':  // monster
      fight(m, p, c);
      break;
    case 'h':  // plus health
      gain_health(m, c);
      break;
    case 't':  // plus time
      gain_time(m, c);
      break;
    case '$':  // plus loot
      bag_loot(m, c);
      break;
    case '1':  // keys
    case '2':
    case '3':
      get_key(m, c);
      break;
    case '.':  // egg
      get_egg(m, c);
      break;
    case '<':  // prev level
      goto_prev(m, p);
      break;
    case '>':  // next level
      goto_next(m, p);
      break;
    case 'o':  // pitfall
      fall(m, p);
      break;
    case '#':  // bomb
      blowup(m, p);
      break;
    default:;
  };
}

/**-----------------------------------------------------------------------------
 * @brief switchboard to get direction input from the keyboard, then
 * display & return it
 */
Dir read_dir(Maze& m)
{
  pair<string, Dir> res{};

  switch (getch()) {  // get user input
    case 'w':
    case KEY_UP:
      res = {"up  ", up};
      break;
    case 'a':
    case KEY_LEFT:
      res = {"lt  ", lt};
      break;
    case 's':
    case KEY_DOWN:
      res = {"dn  ", dn};
      break;
    case 'd':
    case KEY_RIGHT:
      res = {"rt  ", rt};
      break;
    case 'q':
      quit(m);
      break;
    default:
      res = {"none", none};
  };

  auto [s, d]{res};  // unpack the std::pair (C++17)
  mvprintw(1, hdr2_base + 16, "%s", s.c_str());
  return d;
}

/**-----------------------------------------------------------------------------
 * @brief display the matrix of chars that constitute the maze
 * @note this type of iteration approach functions even with a mix of
 * non-regular string sizes (ie, a sparse-shaped matrix)
 */
void draw_maze(Maze& m)
{
  // DESIGN: add 'reveal unexplored' feature to draw_maze() rendering ??

  uint row{}, col{};
  const auto& l{m.curr_lvl()};

  for (const auto& i : l) {                // read strings (rows)
    col = 0;                               //
    for (const auto j : i) {               // read chars (cols)
      mvprintw(row, (col * 2), "%c ", j);  // add extra spaces for readability
      ++col;
    }
    ++row;
  }
}

/**-----------------------------------------------------------------------------
 * @brief display the player's position
 */
void draw_player(const Posn& p)
{
  // match the extra horizontal spacing of the maze's display
  mvprintw(p.i, (p.j * 2), "@");
}

/**-----------------------------------------------------------------------------
 * @brief display the player's status
 */
void draw_status(Maze& m)
{
  // display the objectives flags
  //------------

  string estr{" egg:  "};  // egg
  estr += to_string(m.egg);
  mvprintw(1, hdr2_base, "%s", estr.c_str());

  string kstr{"keys:  "};  // keys
  for (const auto k : m.keys) {
    kstr += to_string(k) + ' ';
  }
  mvprintw(2, hdr2_base, "%s", kstr.c_str());

  string mstr{"mnst:  "};  // monsters
  for (const auto m : m.monsters) {
    mstr += to_string(m) + ' ';
  }
  mvprintw(3, hdr2_base, "%s", mstr.c_str());

  string rstr{"rdls:  "};  // riddles
  for (const auto r : m.riddles) {
    rstr += to_string(r) + ' ';
  }
  mvprintw(4, hdr2_base, "%s", rstr.c_str());

  string lstr{"livs:  "};  // lives
  for (const auto l : m.lives) {
    lstr += to_string(l) + ' ';
  }
  mvprintw(5, hdr2_base, "%s", lstr.c_str());

  // display the status line
  //------------
  mvprintw(6, hdr2_base, "H:%3d  $:%5d  Pts:%6d ", m.health, m.loot, m.points);
}

/**-----------------------------------------------------------------------------
 * @brief wrapper to draw screen elements
 */
void draw(Maze& m, const Posn& p)
{
  draw_maze(m);
  draw_player(p);
  draw_status(m);

  refresh();
}

/**-----------------------------------------------------------------------------
 * @brief determine if the next desired position is traversable
 */
Posn next_posn(Maze& m, const Posn& prev_posn, const Dir dir)
{
  const Posn desired_posn{prev_posn, dir};

  ++m.points;  // +1 point just for attempting to move

  // if the desired position isn't a wall, return it. otherwise, stay in
  // place.
  m.last_move = m.is_wall(desired_posn) ? prev_posn : desired_posn;
  return m.last_move;
}

/**-----------------------------------------------------------------------------
 * @brief the recursive game 'loop'
 */
void proc_events(Maze& m, const Posn& curr_posn)
{
  check_posn(m, curr_posn);
  draw(m, curr_posn);

  // clang-format off
  if (m.have_egg() &&
      m.have_all(m.keys) &&
      m.have_all(m.riddles) &&
      m.have_all(m.monsters) &&
      m.at_maze_exit(curr_posn)) {
    //
    // nice payday :^)
    m.points += 100'000;
    m.loot += 50'000;
    draw_status(m);

    mvprintw(msg_off++, msg_base, "%s", " all objectives complete");
    attron(A_REVERSE);
    mvprintw(msg_off++, msg_base, "%s", "  you win, player one!!  ");

    leave(m);
    return;  // unused
  }
  // clang-format on

  const auto dir{read_dir(m)};
  const auto new_posn{next_posn(m, curr_posn, dir)};

  proc_events(m, new_posn);  // recurses using new position
}

/**-----------------------------------------------------------------------------
 * @brief init ncurses system & print welcome message
 */
void welcome()
{
  initscr();             // initialize ncurses
  keypad(stdscr, true);  // enable arrow keys
  noecho();              // disable key echo
  curs_set(0);           // disable cursor display

  mvprintw(0, 0, "ready player one");
}

/**-----------------------------------------------------------------------------
 * @brief while maze's 'timer_active' flag is true, this will fire periodically
 * @note currently every 1 second
 */
void do_timed_evts(Maze& m)
{
  if (m.bomb_assploded) {
    mvprintw(5, hdr2_base + 16, "%d ", 0);  // counter is 0 for the game's final
                                            // tick if player set the bomb off
  }
  else {
    // take away a 'life tick' from the counter every second in the maze.
    --m.life_tick;
    mvprintw(5, hdr2_base + 16, "%d ", m.life_tick);
  }

  refresh();

  // if counter reaches zero, player loses a life.
  if (m.life_tick <= 0) {
    m.life_tick = m.timer_len;  // reset for a new countdown
    die(m, m.last_move);
  }
}

/**-----------------------------------------------------------------------------
 *   "This example shows a common way to model stateful programs while
 * avoiding [m]any state changes and keeping the functions pure. In
 * real-world programs, things are usually much more complicated. Here, the
 * only thing that needs to be changed is the position of the player.
 * Everything else, including how the player is shown to the user, can be
 * calculated from the way the position changes.
 *   Larger systems have many movable parts. You could create a huge,
 * all-encompassing world structure that you’d re-create every time you needed
 * to change something in it. This would have a big performance overhead (even
 * if you use data structures optimized for functional programming, as covered
 * in chapter 8) and would significantly increase the complexity of your
 * software.
 *   Instead, you’ll usually have some mutable state that you think would be
 * inefficient to always have to copy and pass around, and you’ll model your
 * functions so they return statements about what should be changed in the
 * world instead of always returning new copies of the world. What this
 * approach brings to the table is a clear separation between mutable and
 * pure parts of the system." -p109
 */
int main()
{
  welcome();
  Maze m{};

  // Create a simple timer for the game
  std::thread thr{[&m]() {
    while (m.timer_active) {  // NOTE: leave() unsets this flag
      sleep_for(1s);
      do_timed_evts(m);
    }
  }};

  proc_events(m, m.start_posn());

  thr.join();  // wait for main thread to finish
}

// Copyright (2020)
// License (MIT)  https://opensource.org/licenses/MIT
