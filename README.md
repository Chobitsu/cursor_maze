# cursor_maze
## the most basic of multi-level maze games ever
- text-based, so it'll run anywhere.
- create new maze levels just by arranging certain characters in a file, no
programming required.

### keyboard controls:
- [w,a,s,d] or [arrow keys] - move player
- [q] - quit

### program build dependencies:
- c++17 (should be on your system, i'm currently using g++ 9.2)
- ncurses - Programming library for text-based user interfaces -
    https://invisible-island.net/ncurses/

### download, build, & run:
```
git clone https://gitlab.com/Chobitsu/cursor_maze.git
cd cursor_maze
c++ maze.cpp -std=c++17 -lncurses -lpthread -o maze
./maze
```
- afterwards to update to the latest, starting in the cursor_maze directory:
```
git pull
c++ maze.cpp -std=c++17 -lncurses -lpthread -o maze
./maze
```

### recommended software:
- linux   (just pick one and go with it already, anon! :^)
