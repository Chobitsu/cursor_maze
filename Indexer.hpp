#pragma once

#include <iterator>
#include <utility>

template <typename T>
struct iterator_extractor {
  using type = typename T::iterator;
};

template <typename T>
struct iterator_extractor<const T> {
  using type = typename T::const_iterator;
};

template <typename T>
class Indexer {
 public:
  Indexer(T& t) : container{t}
  {
  }

  class iterator {
    using inner_iterator = typename iterator_extractor<T>::type;

    using inner_reference =
        typename std::iterator_traits<inner_iterator>::reference;

    using reference = std::pair<size_t, inner_reference>;

   public:
    iterator(inner_iterator iit) : it{iit}
    {
    }

    reference operator*() const
    {
      return reference{pos, *it};
    }

    iterator& operator++()
    {
      ++pos;
      ++it;
      return *this;
    }
    iterator operator++(int)
    {
      auto tmp{*this};
      ++*this;
      return tmp;
    }

    bool operator==(iterator const& other) const
    {
      return it == other.it;
    }
    bool operator!=(iterator const& other) const
    {
      return !(*this == other);
    }

   private:
    size_t pos{};
    inner_iterator it{};

  };  // class iterator

  iterator begin() const
  {
    return iterator{std::begin(container)};
  }
  iterator end() const
  {
    return iterator{std::end(container)};
  }

 private:
  T& container;

};  // class Indexer

template <typename T>
Indexer<T> indexed(T& t)
{
  return Indexer<T>{t};
}

// 'borrowed' from:
// https://stackoverflow.com/questions/10962290/find-position-of-element-in-c11-range-based-for-loop
